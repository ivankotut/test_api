<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Status;
use AppBundle\Entity\ToDo;
use AppBundle\Repository\StatusRepository;
use AppBundle\Repository\ToDoRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class StatusController
 * @package AppBundle\Controller\Api
 */
class StatusController extends BaseController
{

    /**
     * @Rest\Get("/api/status", name="status_list")
     *
     * @param StatusRepository $statusRepository
     * @return array
     */
    public function index(StatusRepository $statusRepository): array
    {
       return $statusRepository->findAll();
    }

}