<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Status;
use AppBundle\Entity\ToDo;
use AppBundle\Repository\ToDoRepository;
use Doctrine\ORM\Query\QueryException;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class ToDoController
 * @package AppBundle\Controller\Api
 */
class ToDoController extends BaseController
{

    /**
     * @Rest\Get("/api/to-do", name="to_do_list")
     *
     * @param Request $request
     * @param ToDoRepository $toDoRepository
     * @return array
     */
    public function index(Request $request, ToDoRepository $toDoRepository): array
    {
        try{
            $result = $toDoRepository->getToDos($request);
        } catch (QueryException $e){
            return ['error' => true, 'message' => $e->getMessage()];
        }

       return $result;
    }

    /**
     * @Rest\Get("/api/to-do/{id}", name="to_do_show")
     *
     * @param ToDo $toDo
     * @return ToDo
     */
    public function show(ToDo $toDo): ToDo
    {
        return $toDo;
    }

    /**
     * @Rest\Put("/api/to-do/{id}/status/{status}", name="to_do_change_status")
     *
     * @param ToDo $toDo
     * @param Status $status
     * @ParamConverter("toDo", options={"id" = "id"})
     * @ParamConverter("status", options={"id" = "status"})
     * @return ToDo
     */
    public function changeStatus(ToDo $toDo, Status $status): ToDo
    {
        $toDo->setStatus($status);

        $this->getDoctrine()->getManager()->persist($toDo);
        $this->getDoctrine()->getManager()->flush();

        return $toDo;
    }

}