<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Class AppController
 * @package AppBundle\Controller
 */
class AppController extends Controller
{


    /**
     * @param $token
     * @return bool
     */
    protected function checkCsrfToken($token): bool
    {
        return $this->isCsrfTokenValid('app_token', $token);
    }
}