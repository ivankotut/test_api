<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Controller\AppController;
use AppBundle\Interfaces\OrderIndexInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HelperController
 * @package AppBundle\Controller\Admin
 *
 * @Route("/admin/helper", name="admin_helper_")
 */
class HelperController extends AppController
{

    /**
     * @Route("/change-order/{entity}/{entityId}/{value}", requirements={"value"="\d+"}, methods={"PUT"})
     *
     * @param $entity
     * @param $entityId
     * @param $value
     * @param Request $request
     * @return Response
     */
    public function changeOrder($entity, $entityId, $value, Request $request): Response
    {

        if(!$this->checkCsrfToken($request->request->get('_token'))){
            return $this->json(['status' => false, 'message' => 'Invalid request']);
        }

        $entityNamespace = 'AppBundle\\Entity\\' . $entity;

        if (!class_exists($entityNamespace)) {
            return $this->json(['status' => false, 'message' => 'Class not found!']);
        }

        if (!(new $entityNamespace) instanceof OrderIndexInterface) {
            return $this->json(['status' => false, 'message' => 'Class must by implement OrderIndexInterface']);
        }

        $em = $this->getDoctrine();
        $repo = $em->getRepository($entityNamespace);

        /** @var $entityObject OrderIndexInterface */
        if (!$entityObject = $repo->find($entityId)) {
            return $this->json(['status' => false, 'message' => 'Item not found!']);
        }

        $entityObject->setOrderIndex(abs($value));

        $em->getManager()->persist($entityObject);
        $em->getManager()->flush();

        return $this->json(['status' => true]);
    }
}