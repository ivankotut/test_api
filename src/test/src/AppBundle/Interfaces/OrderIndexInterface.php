<?php

namespace AppBundle\Interfaces;

/**
 * Interface OrderIndexInterface
 * @package AppBundle\Interfaces
 */
interface OrderIndexInterface
{
    /**
     * @return int|null
     */
    public function getOrderIndex(): int;

    /**
     * @param int $orderIndex
     */
    public function setOrderIndex(int $orderIndex);
}