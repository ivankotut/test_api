<?php

namespace AppBundle\Traits;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait OrderIndexTrait
 * @package AppBundle\Entity\Traits
 */
trait OrderIndexTrait
{
    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/\d+/",
     *     message="Is not a number"
     * )
     *
     * @ORM\Column(name="order_index", type="integer", options={"default": 0})
     */
    private $orderIndex = 0;

    /**
     * @return int
     */
    public function getOrderIndex(): int
    {
        return $this->orderIndex;
    }

    /**
     * @param int $orderIndex
     * @return $this
     */
    public function setOrderIndex(int $orderIndex)
    {
        $this->orderIndex = $orderIndex;

        return $this;
    }

}
