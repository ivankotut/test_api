<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Interfaces\OrderIndexInterface;
use AppBundle\Traits\OrderIndexTrait;

/**
 * Status
 *
 * @ORM\Table(name="status")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StatusRepository")
 */
class Status implements OrderIndexInterface
{

    use OrderIndexTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min="3",
     *     max="255"
     * )
     *
     * @ORM\Column(name="title", type="string", length=255, unique=true)
     */
    private $title;

    /**
     * @Assert\NotBlank()
     *
     * @ORM\OneToMany(targetEntity="ToDo", mappedBy="status", cascade={"persist", "remove"})
     */
    private $toDos;

    /**
     * Status constructor.
     */
    public function __construct()
    {
        $this->toDos = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Status
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return ArrayCollection
     */
    public function getToDos(): ArrayCollection
    {
        return $this->toDos;
    }

    /**
     * @param ToDo|null $toDo
     * @return Status
     */
    public function addToDos(?ToDo $toDo): self
    {
        if (!$this->toDos->contains($toDo)) {
            $this->toDos[] = $toDo;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getTitle() ?: 'N/A';
    }
}

