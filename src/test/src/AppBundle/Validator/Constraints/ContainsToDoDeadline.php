<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsToDoDeadline extends Constraint
{
    public $message = '';

    public function validatedBy(): string
    {
        return \get_class($this).'Validator';
    }
}