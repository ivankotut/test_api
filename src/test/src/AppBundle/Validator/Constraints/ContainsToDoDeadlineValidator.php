<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
class ContainsToDoDeadlineValidator extends ConstraintValidator
{

    /**
     * @param mixed $value
     * @param Constraint $constraint
     * @return bool
     */
    public function validate($value, Constraint $constraint)
    {
        $toDoDate = $value;
        $currentDate = new \DateTime();

        if($toDoDate < $currentDate) {
            return $this->context->buildViolation('The date cannot be less than the current date')->addViolation();
        }

        return true;
    }
}