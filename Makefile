#!make
include .env
export $(shell sed 's/=.*//' .env)
RED="\033[0;31m"        	# ${RED}
GREEN="\033[0;32m"      	# ${GREEN}
BOLD="\033[1;m"				# ${BOLD}
WARNING="\033[37;1;41m"		# ${WARNING}
END_COLOR="\033[0m"			# ${END_COLOR}

.PHONY: help docker-env nginx-config symfony-config generate-jwt generate-ssl clone up stop restart rebuild status hosts composer-install composer-update schema-update cache-clear console-backend console-frontend console-db console-web-srv console-redis logs-web-srv logs-backend-php logs-backend-app logs-frontend logs-db logs-redis clean-backend clean-frontend clean-docker clean-all

help:
	@echo "\n\033[1mMain section\033[0m"
	@echo "up\t\t\t- start project"
	@echo "stop\t\t\t- stop project"
	@echo "restart\t\t\t- restart containers"
	@echo "rebuild\t\t\t- build containers w/o cache"
	@echo "status\t\t\t- show status of containers"
	@echo "hosts\t\t\t- add application domains in /etc/hosts file"
	@echo "composer-install\t- install dependencies via composer"
	@echo "composer-update\t\t- update dependencies via composer"
	@echo "schema-update\t\t- update database schema"
	@echo "load-fixtures\t\t- load fixtures into database"
	@echo "cache-clear\t\t- clear application cache data"
	@echo "\033[0;33mhelp\t\t\t- show this menu\033[0m"

	@echo "\n\033[1mConsole section\033[0m"
	@echo "console-db\t\t- run bash console for db container"
	@echo "console-backend\t\t- run bash console for backend container"
	@echo "console-frontend\t- run bash console for frontend container"
	@echo "console-web-srv\t\t- run bash console for web server container"

	@echo "\n\033[1mLogs section\033[0m"
	@echo "logs-db\t\t\t- show db container logs"
	@echo "logs-backend-php\t- show backend container php-fpm logs"
	@echo "logs-backend-app\t- show backend container app logs"
	@echo "logs-frontend\t\t- show frontend container app logs"
	@echo "logs-redis\t\t- show redis container logs"
	@echo "logs-web-srv\t\t- show web server logs"

	@echo "\n\033[1mResetting section\033[0m"
	@echo "\033[1;31m\033[5mclean-backend\t\t- Reset backend repository\033[0m"
	@echo "\033[1;31m\033[5mclean-frontend\t\t- Reset frontend repository\033[0m"
	@echo "\033[1;31m\033[5mclean-docker\t\t- Reset docker repository\033[0m"
	@echo "\033[1;31m\033[5mclean-all\t\t- Reset ALL project data\033[0m"

up:
	@echo "\n\033[1;mSpinning up containers for ${ENVIRONMENT} environment... \033[0m"
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml up -d; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml up -d; fi"
	@$(MAKE) --no-print-directory status

stop:
	@echo "\n\033[1;mHalting containers... \033[0m"
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml stop; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml stop; fi"
	@echo "\n\033[1;mRemoving network... \033[0m"
	@$(MAKE) --no-print-directory status

restart:
	@echo "\n\033[1;mRestarting containers... \033[0m"
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml stop; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml stop; fi"

	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml up -d; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml up -d; fi"
	@$(MAKE) --no-print-directory status

rebuild: stop
	@echo "\n\033[1;mRebuilding containers... \033[0m"
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml build --no-cache; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml build --no-cache; fi"

status:
	@echo "\n\033[1;mContainers statuses \033[0m"
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml ps; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml ps; fi"
	@echo "\n\033[1;mNetwork status \033[0m"
	@bash ./scripts/sh-network-status.sh


setup-editor:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec backend bash -c 'php bin/console ckeditor:install && php bin/console assets:install web'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec -T backend bash -c 'php bin/console ckeditor:install && php bin/console assets:install web; fi"

frontend-init:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec backend bash -c 'npm rebuild && yarn install && npm install'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec -T backend bash -c 'npm rebuild && yarn install && npm install'; fi"
frontend-run-dev:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec backend bash -c 'npm run dev'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec -T backend bash -c 'npm run dev'; fi"

frontend-run-watch:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec backend bash -c 'npm run watch'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec -T backend bash -c 'npm run watch'; fi"

deploy-dev:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then cd src/backend && git pull && cd ../.. && make frontend-init && make frontend-run-dev && make composer-install && make schema-update && make cache-clear && make setup-editor; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ]; then cd src/backend && git pull && cd ../.. && make frontend-init && make frontend-run-dev && make composer-install && make schema-update && make cache-clear && make setup-editor; fi"


hosts:
	@echo "\n\033[1;mAdding record in to your local hosts file.\033[0m"
	@echo "\n\033[1;mPlease use your local sudo password.\033[0m"
	@echo "127.0.0.1 localhost ${SERVER_NAME} api.${SERVER_NAME}" | sudo tee -a /etc/hosts
	@echo "127.0.0.1 localhost www.${SERVER_NAME} www.api.${SERVER_NAME}" | sudo tee -a /etc/hosts

composer-install:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec backend bash -c 'composer install'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec -T backend bash -c 'composer install'; fi"
	@$(MAKE) --no-print-directory permissions
composer-install-admin:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec admin bash -c 'composer install'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec admin bash -c 'composer install'; fi"
	@$(MAKE) --no-print-directory permissions
composer-update:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec backend bash -c 'composer update'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec backend bash -c 'composer update'; fi"
	@$(MAKE) --no-print-directory permissions
composer-update-admin:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec admin bash -c 'composer update'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec admin bash -c 'composer update'; fi"
	@$(MAKE) --no-print-directory permissions

schema-update:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec backend bash -c 'php bin/console doctrine:cache:clear-metadata && php bin/console doctrine:schema:update --force'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec -T backend bash -c 'php bin/console doctrine:cache:clear-metadata && php bin/console doctrine:schema:update --force'; fi"

load-fixtures:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec backend bash -c 'php bin/console doctrine:fixtures:load'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec backend bash -c 'php bin/console doctrine:fixtures:load'; fi"

cache-clear:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec redis bash -c 'redis-cli flushall'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec backend bash -c 'php bin/console cache:clear --env=dev'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec backend bash -c 'php bin/console cache:clear --env=prod'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec -T redis bash -c 'redis-cli flushall'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec -T backend bash -c 'php bin/console cache:clear --env=dev'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec -T backend bash -c 'php bin/console cache:clear --env=prod'; fi"
	@$(MAKE) --no-print-directory permissions

permissions:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec backend bash -c 'chown www-data:www-data -R ./var/ && chmod 755 -R ./var/'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec -T backend bash -c 'chown www-data:www-data -R ./var/ && chmod 755 -R ./var/'; fi"

build-frontend:
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-frontend-builder.yml up; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-frontend-builder.yml rm -sfv; fi"

console-backend:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec backend bash; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec backend bash; fi"
console-siren:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec sirenpublishing bash; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec backend bash; fi"
console-admin:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec admin bash; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec backend bash; fi"
console-frontend:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec frontend bash; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then echo \"FRONTEND container is not active. Your ENVIRONMENT = ${ENVIRONMENT}\"; fi"
console-db:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec db bash; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then echo \"DB container is not active. Your ENVIRONMENT = ${ENVIRONMENT}\"; fi"
console-web-srv:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec web-srv bash; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec web-srv bash; fi"
console-redis:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec redis bash; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec redis bash; fi"

console-es:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec elasticsearch bash; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec redis bash; fi"

logs-web-srv:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml logs --tail=100 -f web-srv; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml logs --tail=100 -f web-srv; fi"
logs-backend-php:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml logs --tail=100 -f backend; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml logs --tail=100 -f backend; fi"
logs-backend-app:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml exec backend bash -c 'tail -f ./var/logs/dev.log'; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml exec backend bash -c 'tail -f ./var/logs/dev.log'; fi"
logs-frontend:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml logs --tail=100 -f frontend; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then echo \"FRONTEND container is not active. Your ENVIRONMENT = ${ENVIRONMENT}\"; fi"
logs-db:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml logs --tail=100 -f db; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then echo \"DB container is not active. Your ENVIRONMENT = ${ENVIRONMENT}\"; fi"
logs-redis:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml logs --tail=100 -f redis; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml logs --tail=100 -f redis; fi"
logs-es:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml logs --tail=100 -f elasticsearch; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml logs --tail=100 -f redis; fi"

clean-backend:
	@bash -c "while true; do echo -ne \"\n\033[1;31m\033[5m*** Resetting backend repository ***\033[0m\n\"; echo -ne \"\033[1;31m\033[5m*** Ensure that you commited changes ***\033[0m\n\"; bash ./scripts/sh-dialog.sh || break; cd ./src/backend && git reset --hard; break; done"
clean-frontend:
	@bash -c "while true; do echo -ne \"\n\033[1;31m\033[5m*** Resetting frontend repository ***\033[0m\n\"; echo -ne \"\033[1;31m\033[5m*** Ensure that you commited changes ***\033[0m\n\"; bash ./scripts/sh-dialog.sh || break; cd ./src/frontend && git reset --hard; break; done"
clean-docker:
	@bash -c "while true; do echo -ne \"\n\033[1;31m\033[5m*** Resetting docker repository ***\033[0m\n\"; echo -ne \"\033[1;31m\033[5m*** Ensure that you commited changes ***\033[0m\n\"; bash ./scripts/sh-dialog.sh || break; git reset --hard; break; done"
clean-all:
	@bash -c "if [ ${ENVIRONMENT} = 'local' ]; then while true; do echo -ne \"\n\033[1;31m\033[5m*** Removing containers and application repositories ***\033[0m\n\"; echo -ne \"\033[1;31m\033[5m*** Ensure that you commited changes ***\033[0m\n\"; bash ./scripts/sh-dialog.sh || break; docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-local.yml down --rmi all 2> /dev/null; git reset --hard; rm -rf ./src; break; done; fi"
	@bash -c "if [ ${ENVIRONMENT} = 'dev' ] || [ ${ENVIRONMENT} = 'stage' ]; then while true; do echo -ne \"\n\033[1;31m\033[5m*** Removing containers and application repositories ***\033[0m\n\"; echo -ne \"\033[1;31m\033[5m*** Ensure that you commited changes ***\033[0m\n\"; bash ./scripts/sh-dialog.sh || break; docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-dev-stage.yml down --rmi all 2> /dev/null; git reset --hard; rm -rf ./src; break; done; fi"
