####deployment
```git pull```

```make rebuild```

```make up```
```make composer-install```
```mske schema-updat```

####add to hosts file
```
127.0.0.1 frontend.test.local.com
127.0.0.1 test.local.com
```

####api methods
```
GET - /api/to-do - show list todo
GET - /api/to-do?sort=asc&column=title&count=10&offset=0 // all params

GET - /api/to-do/{id} - get one todo by id
PUT - /api/to-do/{todoId}/status/{statusId} - change status

GET - /api/status - get all status

```
http://take.ms/m6jul